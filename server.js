// imports for the dependencies
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");

// imports for routes
const users = require("./routes/api/users");
const contacts = require("./routes/api/contacts");
const statistics = require("./routes/api/statistics");

// initialize app
const app = express();

// body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// db config
const db = require("./config/keys").MONGO_URI;

// connect db
mongoose
  .connect(db)
  .then(() => console.log("database connected!"))
  .catch((err) => console.log(err));

// passport middleware
app.use(passport.initialize());

// passport config
require("./config/passport")(passport);

// use created routes
app.use("/api/users", users);
app.use("/api/contacts", contacts);
app.use("/api/statistics", statistics);

// declare a default port
const port = process.env.PORT || 5000;

// start the server
app.listen(port, () => console.log(`server running on port ${port}`));
