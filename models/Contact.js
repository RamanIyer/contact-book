// imports for the dependencies
const mongoose = require("mongoose");

// initialize schema
const Schema = mongoose.Schema;

// create userSchema
const contactSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users',
  },
  name: {
    firstName: {
      type: String,
      required: true,
    },
    middleName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    displayName: {
      type: String,
    }
  },
  primaryNumber: {
    numberType: {
        type: String,
        default: "Mobile",
      },
      countryCode: {
        type: String,
        default: "+91",
      },
      phoneNumber: {
        type: Number,
        required: true,
      }
  },
  secondaryNumbers: [
    {
      numberType: {
        type: String,
        default: "Mobile",
      },
      countryCode: {
        type: String,
        default: "+91",
      },
      phoneNumber: {
        type: Number,
        required: true,
      }
    }
  ],
  address: {
    type: String,
  },
  email: {
    type: String,
  },
  website: {
    type: String,
  },
  events: [
    {
      eventType: {
        type: String,
        default: "Birthday",
      },
      eventDate: {
        type: Date,
        required: true,
      }
    }
  ],
});

// export created schema
module.exports = Contact = mongoose.model("contacts", contactSchema);
