// imports for the dependencies
const mongoose = require("mongoose");

// initialize schema
const Schema = mongoose.Schema;

// create userSchema
const userSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  phone: {
    type: Number,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now(),
  },
});

// export created schema
module.exports = User = mongoose.model("users", userSchema);
