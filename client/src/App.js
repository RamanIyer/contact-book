import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import AppRouter from './AppRouter';
import NavHeader from './components/common/NavHeader';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react';
import setAxiosAuth from './utils/setAxiosAuth';
import '@fortawesome/fontawesome-free/css/all.css';

function App() {
  const [authToken, setAuthToken] = useState('');

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token && token !== '') {
      setAuthToken(token);
      setAxiosAuth(token);
    }
  }, [])

  const setToken = (token) => {
    if (token === '') {
      localStorage.removeItem("token");
      setAuthToken('');
      setAxiosAuth('');
    }
    else{
      localStorage.setItem("token", token.token);
      setAuthToken(token.token);
      setAxiosAuth(token.token);
    }
  };

  return (
    <div className="app">
      <Router>
        <NavHeader isLogin={authToken !== ''} setToken={setToken} />
        <div className="content-wrapper">
          <AppRouter isLogin={authToken !== ''} setToken={setToken} />
        </div>
      </Router>
    </div>
  );
}

export default App;
