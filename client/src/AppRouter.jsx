import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { ROUTES } from './utils/constants';

import PageNotFound from './pages/page-not-found';
import HomePage from './pages/home';
import EditContactPage from './pages/edit-contact';
import ContactsPage from './pages/contacts';
import LoginPage from './pages/login';
import RegisterPage from './pages/register';
import ContactDetailPage from './pages/contact-detail';
import EventsPage from './pages/events';

const AppRouter = ({ isLogin, setToken }) => {
  return (
    <Switch>
      <Route exact path={ROUTES.NOT_FOUND} component={PageNotFound} />
      <Route exact path={ROUTES.LOGIN} render={() => <LoginPage setToken={setToken} />}>
        {isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.REGISTER} component={RegisterPage}>
        {isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.HOME} component={HomePage}>
        {isLogin && <Redirect to={ROUTES.CONTACTS} />}
      </Route>
      <Route exact path={ROUTES.CONTACTS} component={ContactsPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.CONTACT_DETAIL} component={ContactDetailPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.ADD_CONTACT} component={EditContactPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.EDIT_CONTACT} component={EditContactPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.EVENTS} component={EventsPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Redirect to={ROUTES.NOT_FOUND} />
    </Switch>
  );
};

export default AppRouter;