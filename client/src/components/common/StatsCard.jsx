import React from 'react';

const StatsCard = (({count, entity, message}) => {
    return (
        <div className="stats-card">
            <h1 className="stats-count mb-4">{count}</h1>
            <h4 className="stats-entity mb-2">{entity}</h4>
            <p>{message}</p>
        </div>
    )
});

export default StatsCard;