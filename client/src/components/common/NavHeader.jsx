import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';

const NavHeader = ({ isLogin, setToken }) => {
  const history = useHistory();

  const userLogout = () => {
    setToken('');
    history.push('/');
  }

  return (
    <div className="navbar-header">
      <Navbar>
        <Nav className="mr-auto">
          <Navbar.Brand>
            <Link className="navbar-link app-name" to="/">Contact Book</Link>
          </Navbar.Brand>
        </Nav>
        <Nav>
          {isLogin ?
            (<>
              <Nav.Item>
                <Link className="navbar-link" to="/events">Events</Link>
              </Nav.Item>
              <Nav.Item onClick={userLogout}>
                <Link className="navbar-link" to="/home">Logout</Link>
              </Nav.Item>
            </>) :
            (<>
              <Nav.Item>
                <Link className="navbar-link" to="/login">Login</Link>
              </Nav.Item>
              <Nav.Item>
                <Link className="navbar-link" to="/register">Register</Link>
              </Nav.Item>
            </>)
          }
        </Nav>
      </Navbar>
    </div>
  )
};

export default NavHeader;