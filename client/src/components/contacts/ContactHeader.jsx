import React from 'react';

const ContactHeader = (({alphabet}) => {
  return (
    <div className="mb-4">
      <div>
        <span className="alphabet-seperator">{alphabet}</span>
      </div>
    </div>
  );
});

export default ContactHeader;