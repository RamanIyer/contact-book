import React from 'react';
import { Col, Row, Image, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import placeholder from '../../resources/placeholder.jpg';

const ContactCard = (({ id, name, number, email }) => {
  return (
    <div className="col-sm-4">
        <Row className="contacts-card">
            <Col sm="4" className="text-right">
                <Image src={placeholder} roundedCircle height="150px"/>
            </Col>
            <Col sm="6" className="text-left">
                <div>
                      <p className="mb-3 contact-card-name">{name}</p>
                </div>
                <h3 className="mb-3">{number}</h3>
                {
                    email !== '' && (
                        <h5>{email}</h5>
                    )
                }
            </Col>
            <Col sm="2">
                  <Link to={`/contacts/contact/${id}`}>
                    <Button variant="light"><i className="fas fa-ellipsis-v"></i></Button>
                </Link>
            </Col>
        </Row>
    </div>
  );
});

export default ContactCard;