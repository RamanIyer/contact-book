import React from 'react';

const CountryCodes = ({id, value, onChangeHandler, index}) => {
    return(
        <select className="input-field cc-dropdown" data-index={index} value={value} id={id} onChange={onChangeHandler}>
            <option value="+91">+91</option>
            <option value="+1">+1</option>
            <option value="+61">+61</option>
            <option value="+32">+32</option>
            <option value="+86">+86</option>
        </select>
    )
};

export default CountryCodes;