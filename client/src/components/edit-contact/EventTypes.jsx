import React from 'react';

const EventTypes = ({id, value, onChangeHandler, index}) => {
    return(
        <select className="input-field event-type-dropdown" data-index={index} value={value} id={id} onChange={onChangeHandler}>
            <option value="Birthday">Birthday</option>
            <option value="Anniversary">Anniversary</option>
            <option value="Friendship">Friendship</option>
            <option value="Other">Other</option>
        </select>
    )
};

export default EventTypes;