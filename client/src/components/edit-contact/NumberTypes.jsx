import React from 'react';

const NumberTypes = ({id, value, onChangeHandler, index}) => {
    return(
        <select className="input-field type-dropdown" data-index={index} value={value} id={id} onChange={onChangeHandler}>
            <option value="Mobile">Mobile</option>
            <option value="Work">Work</option>
            <option value="Home">Home</option>
            <option value="Main">Main</option>
            <option value="Other">Other</option>
        </select>
    )
};

export default NumberTypes;