import axios from 'axios';

const setAxiosAuth = (token) => {
    if (token && token !== '') {
        axios.defaults.headers.common['Authorization'] = token;
    }
    else {
        delete axios.defaults.headers.common['Authorization'];
    }
}

export default setAxiosAuth;