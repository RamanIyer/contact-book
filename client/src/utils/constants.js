export const ROUTES = {
  HOME: '/',
  NOT_FOUND: '/404',
  LOGIN: '/login',
  REGISTER: '/register',
  CONTACTS: '/contacts',
  ADD_CONTACT: '/contacts/add',
  EDIT_CONTACT: '/contacts/edit/:contact_id',
  EVENTS: '/events',
  CONTACT_DETAIL: '/contacts/contact/:contact_id'
};