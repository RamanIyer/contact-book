import React, { useEffect, useState } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import StatsCard from '../../components/common/StatsCard';
import axios from 'axios';

const HomePage = () => {
  const [userCount, setUserCount] = useState(0);
  const [contactCount, setContactCount] = useState(0);

  useEffect(() => {
    axios.get("/api/statistics/allUsers")
      .then(res => setUserCount(res.data));

    axios.get("/api/statistics/allContacts")
      .then(res => setContactCount(res.data))
  }, [])

  return (
    <div className="home-page">
      <h1 className="page-heading mt-3 mb-4">Contact Book</h1>
      <div className="app-description mb-5">
        <h5>
          Save, Update, Delete Contacts using this app.
        </h5>
        <h5>
          Create an account if not already created and start registering contacts. Never worry about forgetting the numbers again.
        </h5>
        <h5>
            Shown below is the success of our site so far. Join today and be a part of that success!
        </h5>
      </div>
      <Row className="mb-5">
        <Col sm="6">
          <StatsCard count={userCount} entity="users" message="Have registered With Us." />
        </Col>
        <Col sm="6">
          <StatsCard count={contactCount} entity="contacts" message="Have Been Created" />
        </Col>
      </Row>
      <Button variant="black" className="m-3">
        <Link className="navbar-link" to="/login">Login</Link>
      </Button>
      <Button variant="black" className="m-3">
        <Link className="navbar-link" to="/register">Register</Link>
      </Button>
    </div>
  );
}

export default HomePage;
