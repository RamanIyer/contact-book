import React, { useState, useEffect } from 'react';
import { Col, Row } from 'react-bootstrap';
import StatsCard from '../../components/common/StatsCard';
import axios from 'axios';

const EventsPage = () => {
  const [eventCount, setEventCount] = useState(0);
  const [contactCount, setContactCount] = useState(0);
  const [events, setEvents] = useState([]);

  useEffect(() => {
    axios.get("/api/statistics/eventCounts")
      .then(res => setEventCount(res.data));

    axios.get("/api/statistics/userContacts")
      .then(res => setContactCount(res.data));
    
    axios.get("/api/contacts/allEvents")
      .then(res => setEvents(res.data));
  }, []);
  return (
    <div className="events-page">
      <h1 className="page-heading mt-3 mb-4">Events</h1>
      <Row className="mb-5">
        <Col sm="6">
          <StatsCard count={contactCount} entity="contacts" message="Have Been Added By You." />
        </Col>
        <Col sm="6">
          <StatsCard count={eventCount} entity="events" message="Have Been Created By You" />
        </Col>
      </Row>
      <h1>Upcoming Events</h1>
      {
        events.length ?
        (
          <ul className="event-list">
            {
              events.map(event => {
                return <li>{event.eventDate} : {event.eventType} of {event.name}</li>
              })
            }
          </ul>
        ) :
        (
          <div>No Events Have Been Created Yet!</div>
        )
      }
    </div>
  );
}

export default EventsPage;
