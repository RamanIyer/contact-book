import axios from 'axios';
import React, { useState, useEffect }  from 'react';
import { Button } from 'react-bootstrap';
import { Link, useHistory, useParams } from 'react-router-dom';
import CountryCodes from '../../components/edit-contact/CountryCodes';
import EventTypes from '../../components/edit-contact/EventTypes';
import NumberTypes from '../../components/edit-contact/NumberTypes';

const EditContactPage = () => {
  const history = useHistory();
  const { contact_id: contactId } = useParams();
  const [firstName, setFirstName] = useState('');
  const [middleName, setMiddleName] = useState('');
  const [lastName, setLastName] = useState('');
  const [displayName, setDisplayName] = useState('');
  const [primaryNumber, setPrimaryNumber] = useState(
    {
      numberType: 'Mobile',
      countryCode: '+91',
      phoneNumber: ''
    }
  );
  const [secondaryNumbers, setSecondaryNumbers] = useState([]);
  const [address, setAddress] = useState('');
  const [email, setEmail] = useState('');
  const [website, setWebsite] = useState('');
  const [events, setEvents] = useState([]);
  const [errors, setErrors] = useState({});

  useEffect(() => {
    if(contactId) {
      axios.get(`/api/contacts/contact/${contactId}`)
        .then(res => {
          const {
            name,
            primaryNumber,
            secondaryNumbers,
            address,
            email,
            website,
            events
          } = res.data;

          setFirstName(name.firstName);
          setMiddleName(name.middleName);
          setLastName(name.lastName);
          setDisplayName(name.displayName);
          setPrimaryNumber(primaryNumber);
          setSecondaryNumbers(secondaryNumbers);
          setAddress(address);
          setEmail(email);
          setWebsite(website);
          setEvents(events);
        })
    }
  },[contactId]);

  const onChangeHandler = (e) => {
    const id = e.target.id;
    const value = e.target.value;

    switch(id){
      case "firstName": {
        setFirstName(value);
        break;
      }
      case "middleName": {
        setMiddleName(value);
        break;
      }
      case "lastName": {
        setLastName(value);
        break;
      }
      case "displayName": {
        setDisplayName(value);
        break;
      }
      case "primaryNumberType": {
        const updatedPrimaryNumber = {...primaryNumber};
        updatedPrimaryNumber.numberType = value;
        setPrimaryNumber(updatedPrimaryNumber);
        break;
      }
      case "primaryCountryCode": {
        const updatedPrimaryNumber = {...primaryNumber};
        updatedPrimaryNumber.countryCode = value;
        setPrimaryNumber(updatedPrimaryNumber);
        break;
      }
      case "primaryPhoneNumber": {
        const updatedPrimaryNumber = {...primaryNumber};
        updatedPrimaryNumber.phoneNumber = value;
        setPrimaryNumber(updatedPrimaryNumber);
        break;
      }
      case "secondaryNumberType": {
        const index = parseInt(e.target.getAttribute("data-index"));
        const updatedsecondaryNumbers = [...secondaryNumbers];
        updatedsecondaryNumbers[index].numberType = value;
        setSecondaryNumbers(updatedsecondaryNumbers);
        break;
      }
      case "secondaryCountryCode": {
        const index = parseInt(e.target.getAttribute("data-index"));
        const updatedsecondaryNumbers = [...secondaryNumbers];
        updatedsecondaryNumbers[index].countryCode = value;
        setSecondaryNumbers(updatedsecondaryNumbers);
        break;
      }
      case "secondaryPhoneNumber": {
        const index = parseInt(e.target.getAttribute("data-index"));
        const updatedsecondaryNumbers = [...secondaryNumbers];
        updatedsecondaryNumbers[index].phoneNumber = value;
        setSecondaryNumbers(updatedsecondaryNumbers);
        break;
      }
      case "address": {
        setAddress(value);
        break;
      }
      case "email": {
        setEmail(value);
        break;
      }
      case "website": {
        setWebsite(value);
        break;
      }
      case "eventType": {
        const index = parseInt(e.target.getAttribute("data-index"));
        const updatedEvents = [...events];
        updatedEvents[index].eventType = value;
        setEvents(updatedEvents);
        break;
      }
      case "eventDate": {
        const index = parseInt(e.target.getAttribute("data-index"));
        const updatedEvents = [...events];
        updatedEvents[index].eventDate = value;
        setEvents(updatedEvents);
        break;
      }
      default: break;
    }
  };

  const addSecondaryPhone = () => {
    const updatedsecondaryNumbers = [...secondaryNumbers];
    updatedsecondaryNumbers.push(
      {
        numberType: 'Mobile',
        countryCode: '+91',
        phoneNumber: ''
      }
    )
    setSecondaryNumbers(updatedsecondaryNumbers);
  };

  const removeSecondaryPhone = (e) => {
    const updatedsecondaryNumbers = [...secondaryNumbers];
    const index = e.target.getAttribute("data-index");
    updatedsecondaryNumbers.splice(index, 1);
    setSecondaryNumbers(updatedsecondaryNumbers);
  };

  const addEvent = () => {
    const updatedEvents = [...events];
    updatedEvents.push(
      {
        eventType: 'Birthday',
        eventDate: '',
      }
    );
    setEvents(updatedEvents);
  };

  const removeEvent = (e) => {
    const updatedEvents = [...events];
    const index = e.target.getAttribute("data-index");
    updatedEvents.splice(index, 1);
    setEvents(updatedEvents);
  };

  const buildContactDetails = () => {
    const contactDetails = {};
    contactDetails.name = {};
    contactDetails.name.firstName = firstName;
    contactDetails.name.middleName = middleName;
    contactDetails.name.lastName = lastName;
    contactDetails.name.displayName = displayName;
    contactDetails.primaryNumber = primaryNumber;
    contactDetails.secondaryNumbers = secondaryNumbers.filter((number) => {
      return number.phoneNumber !== '';
    });
    contactDetails.address = address;
    contactDetails.email = email;
    contactDetails.website = website;
    contactDetails.events = events.filter((event) => {
      return event.eventDate !== '';
    });
    return contactDetails;
  }

  const addContact = () => {
    axios.post("/api/contacts/create", buildContactDetails())
      .then(res => {
        const contactId = res.data._id;
        history.push(`/contacts/contact/${contactId}`);
      })
      .catch(err => setErrors(err.response.data));
  };

  const updateContact = () => {
    axios.post(`/api/contacts/edit/${contactId}`, buildContactDetails())
      .then(res => {
        history.push(`/contacts/contact/${contactId}`);
      })
      .catch(err => setErrors(err.response.data));
  };

  const formatDate = (dateString) => {
    const newDate = new Date(dateString);
    return `${newDate.getFullYear()}-${(newDate.getMonth() + 1).toString().length === 1 ? '0' + (newDate.getMonth() + 1) : (newDate.getMonth() + 1)}-${newDate.getDate().toString().length === 1 ? '0' + newDate.getDate() : newDate.getDate()}`;
  };

  return (
    <div className="events-page">
      <h1 className="page-heading mt-3 mb-4">{contactId ? "Edit Contact" : "Add Contact"}</h1>
      <h3>Name</h3>
      <input className="input-field" placeholder="First Name" value={firstName} id="firstName" onChange={onChangeHandler} />
      <div className="errors">
        {errors.firstName}
      </div>
      <input className="input-field" placeholder="Middle Name" value={middleName} id="middleName" onChange={onChangeHandler} />
      <input className="input-field no-error" placeholder="Last Name" value={lastName} id="lastName" onChange={onChangeHandler} />
      <input className="input-field no-error" placeholder="Display Name" value={displayName} id="displayName" onChange={onChangeHandler} />
      <h3 className="mt-4">Contact Details</h3>
      <h4>Primary</h4>
      <div className="mb-4">
        <NumberTypes value={primaryNumber.numberType} id="primaryNumberType" onChangeHandler={onChangeHandler} />
        <CountryCodes value={primaryNumber.countryCode} id="primaryCountryCode" onChangeHandler={onChangeHandler} />
        <input maxLength="10" className="input-field phone" placeholder="Phone Number" value={primaryNumber.phoneNumber} id="primaryPhoneNumber" onChange={onChangeHandler} />
      </div>
      <div className="errors">
        {errors.phone}
      </div>
      {secondaryNumbers.length > 0 && (
        <>
          <h4 className="mt-3">Secondary</h4>
          <div className="mb-4">
            {
              secondaryNumbers.map((detail, index) => {
                return (
                  <div key={index}>
                    <NumberTypes value={detail.numberType} index={index} id="secondaryNumberType" onChangeHandler={onChangeHandler} />
                    <CountryCodes value={detail.countryCode} index={index} id="secondaryCountryCode" onChangeHandler={onChangeHandler} />
                    <input maxLength="10" className="input-field phone secondary-phone" placeholder="Phone Number" value={detail.phoneNumber} data-index={index} id="secondaryPhoneNumber" onChange={onChangeHandler} />
                    <Button variant="black-close" data-index={index} onClick={removeSecondaryPhone}>X</Button>
                  </div>
                )
              })
            }
          </div>
          <div className="errors">
            {errors.secondaryPhone}
          </div>
        </>
      )}
      <Button variant="black" onClick={addSecondaryPhone}>Add Secondary Number</Button>
      <h3 className="mt-4">Communication</h3>
      <textarea className="input-field text-area no-error" placeholder="Address" value={address} id="address" onChange={onChangeHandler} />
      <input className="input-field no-error" placeholder="E-Mail" value={email} id="email" onChange={onChangeHandler} />
      <div className="errors">
        {errors.email}
      </div>
      <input className="input-field" placeholder="Website" value={website} id="website" onChange={onChangeHandler} />
      <div className="errors">
        {errors.website}
      </div>
      {events.length > 0 && (
        <>
          <h3 className="mt-4">Events</h3>
          <div className="mb-4">
            {
              events.map((event, index) => {
                return (
                  <div key={index}>
                    <EventTypes value={event.eventType} index={index} id="eventType" onChangeHandler={onChangeHandler} />
                    <input type="date" className="input-field date-field" data-index={index} value={formatDate(event.eventDate)} id="eventDate" onChange={onChangeHandler} />
                    <Button variant="black-close" data-index={index} onClick={removeEvent}>X</Button>
                  </div>
                )
              })
            }
          </div>
        </>
      )}
      <Button variant="black" onClick={addEvent}>Add Event</Button>
      <div>
        {contactId ? (
          <Button variant="black" onClick={updateContact}>Update Contact</Button> 
        ) : (
          <Button variant="black" onClick={addContact}>Add Contact</Button> 
        )}
      </div>
      <div>
        {contactId ? (
          <Link to={`/contacts/contact/${contactId}`}>
            <Button variant="black" onClick={updateContact}>Cancel</Button>
          </Link>
        ) : (
          <Link to={`/contacts`}>
            <Button variant="black" onClick={addContact}>Cancel</Button> 
          </Link>  
        )}
      </div>
    </div>
  )
}

export default EditContactPage;
