import React from 'react';
import { Link } from 'react-router-dom';

const PageNotFound = () => {
  return (
    <div className="not-found-page">
      <h1 className="page-heading mt-3 mb-4">Page Not Found</h1>
      <p className="login-register-footer">Go To <Link className="general-link" to="/">Home Page</Link></p>
    </div>
  );
}

export default PageNotFound;
