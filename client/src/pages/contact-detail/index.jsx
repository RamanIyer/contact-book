import React, { useEffect, useState } from 'react';
import { Image, Button, Modal } from 'react-bootstrap';
import { Link, useHistory, useParams } from 'react-router-dom';
import placeholder from '../../resources/placeholder.jpg';
import axios from 'axios';

const ContactDetailPage = () => {
  const { contact_id: contactId } = useParams();
  const history = useHistory();
  const [firstName, setFirstName] = useState("");
  const [middleName, setMiddleName] = useState("");
  const [lastName, setLastName] = useState("");
  const [displayName, setDisplayName] = useState("");
  const [primaryNumber, setPrimaryNumber] = useState({
    numberType: "Mobile",
    countryCode: "+91",
    phoneNumber: "",
  });
  const [secondaryNumbers, setSecondaryNumbers] = useState([]);
  const [address, setAddress] = useState("");
  const [email, setEmail] = useState("");
  const [website, setWebsite] = useState("");
  const [events, setEvents] = useState([]);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    axios.get(`/api/contacts/contact/${contactId}`)
      .then(res => {
        const {
            name,
            primaryNumber,
            secondaryNumbers,
            address,
            email,
            website,
            events
          } = res.data;

          setFirstName(name.firstName);
          setMiddleName(name.middleName);
          setLastName(name.lastName);
          setDisplayName(name.displayName);
          setPrimaryNumber(primaryNumber);
          setSecondaryNumbers(secondaryNumbers);
          setAddress(address);
          setEmail(email);
          setWebsite(website);
          setEvents(events);
      });
  }, [contactId]);

  const getContactName = ((firstName, middleName, lastName, displayName) => {
    if (displayName && displayName !== '') return displayName;

    let fullName = firstName;
    fullName += middleName && middleName !== '' ? (' ' + middleName) : "";
    fullName += lastName && lastName !== '' ? (' ' + lastName) : "";

    return fullName;
  });

  const getDisplayDate = dateString => {
    const eventDate = new Date(dateString);
    return `${eventDate.getDate().toString().padStart(2, 0)}/${(eventDate.getMonth() + 1).toString().padStart(2, 0)}/${eventDate.getFullYear()}`;
  };

  const deleteContact = () => {
    axios.delete(`/api/contacts/delete/${contactId}`)
      .then(res => {
        setShowModal(false);
        history.push("/contacts");
      });
  };

  return (
    <>
      <div className="contact-details-page">
        <h1 className="page-heading mt-3 mb-4">Contact Details</h1>
        <div className="mb-4">
          <Link to="/contacts">
            <Button variant="black">Back To Contacts</Button>
          </Link>
        </div>
        <Image src={placeholder} roundedCircle height="200px" />
        <h2>{getContactName(firstName, middleName, lastName, displayName)}</h2>
        <h2>{primaryNumber.countryCode + ' ' + primaryNumber.phoneNumber.toString()}</h2>
        <div className="mb-4">
          <Link to={`/contacts/edit/${contactId}`}>
            <Button variant="black" className="mr-4">Edit Contact</Button>
          </Link>
          <Button variant="black" onClick={() => {setShowModal(true)}}>Delete Contact</Button>
        </div>
        <table className="contact-details-table mb-4">
          <tbody>
            <tr>
              <td colSpan="4" className="heading-row">
                <h3>Name</h3>
              </td>
            </tr>
            <tr>
              <td className="key">First Name</td>
              <td className="separator">:</td>
              <td className="spacer"></td>
              <td className="value">{firstName}</td>
            </tr>
            {(middleName && middleName !== '') && (
              <tr>
                <td className="key">Middle Name</td>
                <td className="separator">:</td>
                <td className="spacer"></td>
                <td className="value">{middleName}</td>
              </tr>
            )}
            {(lastName && lastName !== '') && (
              <tr>
                <td className="key">Last Name</td>
                <td className="separator">:</td>
                <td className="spacer"></td>
                <td className="value">{lastName}</td>
              </tr>
            )}
            {(displayName && displayName !== '') && (
              <tr>
                <td className="key">Display Name</td>
                <td className="separator">:</td>
                <td className="spacer"></td>
                <td className="value">{displayName}</td>
              </tr>
            )}
            <tr>
              <td colSpan="4" className="heading-row">
                <h3>Contact</h3>
                <h4>Primary</h4>
              </td>
            </tr>
            <tr>
              <td className="key">{primaryNumber.numberType}</td>
              <td className="separator">:</td>
              <td className="spacer"></td>
              <td className="value">{primaryNumber.countryCode + ' ' + primaryNumber.phoneNumber.toString()}</td>
            </tr>
            {secondaryNumbers.length > 0 && (
              <>
                <tr>
                  <td colSpan="4" className="heading-row">
                    <h4>Secondary</h4>
                  </td>
                </tr>
                {
                  secondaryNumbers.map(secondaryNumber => {
                    return (
                      <tr>
                        <td className="key">{secondaryNumber.numberType}</td>
                        <td className="separator">:</td>
                        <td className="spacer"></td>
                        <td className="value">{secondaryNumber.countryCode + ' ' + secondaryNumber.phoneNumber.toString()}</td>
                      </tr>
                    )
                  })
                }
              </>
            )}
            {((address && address !== '') || (email && email !== '') || (website && website !== '')) && (
              <>
                <tr>
                  <td colSpan="4" className="heading-row">
                    <h3>Communication</h3>
                  </td>
                </tr>
                {(address && address !== '') && (
                  <tr>
                    <td className="key">Address</td>
                    <td className="separator">:</td>
                    <td className="spacer"></td>
                    <td className="value">{address}</td>
                  </tr>
                )}
                {(email && email !== '') && (
                  <tr>
                    <td className="key">Email</td>
                    <td className="separator">:</td>
                    <td className="spacer"></td>
                    <td className="value">{email}</td>
                  </tr>
                )}
                {(website && website !== '') && (
                  <tr>
                    <td className="key">Website</td>
                    <td className="separator">:</td>
                    <td className="spacer"></td>
                    <td className="value">{website}</td>
                  </tr>
                )}
              </>
            )}
            {events.length > 0 && (
              <>
                <tr>
                  <td colSpan="4" className="heading-row">
                    <h3>Events</h3>
                  </td>
                </tr>
                {
                  events.map(event => {
                    return (
                      <tr>
                        <td className="key">{event.eventType}</td>
                        <td className="separator">:</td>
                        <td className="spacer"></td>
                        <td className="value">{getDisplayDate(event.eventDate)}</td>
                      </tr>
                    )
                  })
                }
              </>
            )}
          </tbody>
        </table>
      </div>
      <Modal show={showModal} onHide={() => { setShowModal(false) }}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Contact</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are You Sure You Want To Delete The Contact?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={deleteContact}>Yes</Button>
          <Button onClick={() => setShowModal(false)}>No</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ContactDetailPage;
