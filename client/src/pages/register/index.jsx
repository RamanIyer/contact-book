import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';

const RegisterPage = () => {
  const history = useHistory();
  const [errors, setErrors] = useState({});
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");

  const registerUser = () => {
    const newUser = {};
    newUser.name = name;
    newUser.email = email;
    newUser.phone = phone;
    newUser.password = password;
    newUser.password2 = password2;

    axios.post("/api/users/register", newUser)
      .then(user => history.push("/login"))
      .catch(err => setErrors(err.response.data));
  };

  return(
    <div className="register-page">
      <h1 className="page-heading mt-3 mb-4">Register</h1>
      <input className="input-field" placeholder="User Name" value={name} onChange={(e) => setName(e.target.value)} />
      <div className="errors">
        {errors.name}
      </div>
      <input className="input-field" placeholder="E-Mail ID" value={email} onChange={(e) => setEmail(e.target.value)} />
      <div className="errors">
        {errors.email}
      </div>
      <input className="input-field" placeholder="Phone" value={phone} onChange={(e) => setPhone(e.target.value)} />
      <div className="errors">
        {errors.phone}
      </div>
      <input type="password" className="input-field" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
      <div className="errors">
        {errors.password}
      </div>
      <input type="password" className="input-field" placeholder="Re-Enter Password" value={password2} onChange={(e) => setPassword2(e.target.value)} />
      <div className="errors">
        {errors.password2}
      </div>
      <Button variant="black" onClick={registerUser}>REGISTER</Button>
      <p className="login-register-footer">Already have an account? Click here to <Link className="general-link" to="/login">Login!</Link></p>
    </div>
  );
}

export default RegisterPage;
