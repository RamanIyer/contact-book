import React, { useState, useEffect } from 'react';
import { Row, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ContactCard from '../../components/contacts/ContactCard';
import ContactHeader from '../../components/contacts/ContactHeader';
import axios from 'axios';

const ContactsPage = () => {
  const [contacts, setContacts] = useState({});

  const nameSorter = (contact1, contact2) => {
    if (contact1.name === contact2.name) return 0;
    return contact1.name < contact2.name ? -1 : 1;
  };

  const getContactName = ((name) => {
    if (name.displayName && name.displayName !== '') return name.displayName;

    let fullName = name.firstName;
    fullName += name.middleName && name.middleName !== '' ? (' ' + name.middleName) : "";
    fullName += name.lastName && name.lastName !== '' ? (' ' + name.lastName) : "";

    return fullName;
  });

  useEffect(() => {
    const contactsHolder = {};
    axios.get("/api/contacts/all").then(res => {
      const allContacts = res.data.map(contact => {
        return {
          id: contact._id,
          name: getContactName(contact.name),
          number: contact.primaryNumber.countryCode + ' ' + contact.primaryNumber.phoneNumber.toString(),
          email: contact.email && contact.email !== '' ? contact.email : '',
        }
      });
      allContacts.sort(nameSorter);
      allContacts.forEach(contact => {
        const alphabet = contact.name[0].toLowerCase();
        if (contactsHolder[alphabet]) contactsHolder[alphabet].push(contact);
        else {
          contactsHolder[alphabet] = [];
          contactsHolder[alphabet].push(contact);
        }
      });
      Object.keys(contactsHolder).forEach(alphabet => {
        const contactList = contactsHolder[alphabet];
        const result = new Array(Math.ceil(contactList.length / 3))
          .fill()
          .map(_ => contactList.splice(0, 3));
        contactsHolder[alphabet] = result;
      });
      setContacts(contactsHolder);
    });
  }, []);

  const renderContacts = () => {
    const allContacts = [];
    Object.keys(contacts).forEach(alphabet => {
      const contactList = contacts[alphabet];
      allContacts.push(
        <>
          <ContactHeader alphabet={alphabet.toUpperCase()} />
          {
            contactList.map((contactRow, rowIndex) => {
              return (
                <Row className="mb-5" key={rowIndex}>
                  {
                    contactRow.map((contactCard) => {
                      const { id, name, number, email } = contactCard;
                      return <ContactCard key={id} id={id} name={name} number={number} email={email} />
                    })
                  }
                </Row>
              )
            })
          }
        </>
      )
    });
    return allContacts;
  };

  return(
    <div className="contacts-page">
      <h1 className="page-heading mt-3 mb-4">Contacts</h1>
      <Link to="/contacts/add"><Button variant="floating-button">+</Button></Link>
      {
        renderContacts()
      }
    </div>
  );
}

export default ContactsPage;
