// imports for the dependencies
const express = require("express");
const passport = require("passport");

// initialize router
const router = express.Router();

// imports for database
const Contact = require("../../models/Contact");
const User = require("../../models/User");

// @route   GET api/statistics/allUsers
// @desc    Return count of the users registered
// @access  Public
router.get("/allUsers", (req, res) => {
  User.countDocuments({}).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// @route   GET api/statistics/allContacts
// @desc    Return count of the contacts added
// @access  Public
router.get("/allContacts", (req, res) => {
  Contact.countDocuments({}).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// @route   GET api/statistics/userContacts
// @desc    Return count of the contact added by one user
// @access  Private
router.get("/userContacts", passport.authenticate("jwt", { session: false }), (req, res) => {
  Contact.countDocuments({user: req.user.id}).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// @route   GET api/statistics/eventCounts
// @desc    Return count of the events added by one user
// @access  Private
router.get("/eventCounts", passport.authenticate("jwt", { session: false }), (req, res) => {
  Contact.find({ user: req.user.id })
    .then(contacts => {
      if (!contacts) {
        return res.json(0);
      }
      
      const eventCounter = (count, contact) => {
        if (contact.events) return count + contact.events.length;
        return count;
      };
      const eventCount = contacts.reduce(eventCounter, 0);
      return res.json(eventCount);
    })
    .catch(() => {
      return res.json(0);
    });
});

// export the router
module.exports = router;
