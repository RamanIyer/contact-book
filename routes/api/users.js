// imports for the dependencies
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");

// initialize router
const router = express.Router();

// imports for validators
const validateRegisterInput = require('../../validation/register');
const validateLoginInput = require("../../validation/login");

// imports for database
const User = require("../../models/User");

// @route   POST api/users/register
// @desc    Register a new user
// @access  Public
router.post("/register", (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      errors.email = "E-mail already registered!";
      return res.status(400).json(errors);
    } else {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        phone: req.body.phone,
      });

      bcrypt.genSalt(10, (err, salt) => {
        if (err) throw err;
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then((user) => res.json(user))
            .catch((err) => console.log(err));
        });
      });
    }
  });
});

// @route   POST api/users/login
// @desc    Logs in the user
// @access  Public
router.post("/login", (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  User.findOne({ email })
    .then((user) => {
      if (!user) {
        errors.email = "User not found!";
        return res.status(400).json(errors);
      }

      bcrypt.compare(password, user.password).then((isMatch) => {
        if (isMatch) {
          const payload = {
            id: user.id,
            name: user.name,
            email: user.email,
            phone: user.phone,
          }
          const secret = require('../../config/keys').SECRET_OR_KEY;
          jwt.sign(payload, secret, { expiresIn: 86400 }, (err, token) => {
            if (err) throw err;
            res.json({
              success: true,
              token: 'Bearer ' + token,
            })
          })
        } else {
          errors.password = "Password is incorrect!";
          res.status(400).json(errors);
        }
      });
    })
    .catch((err) => console.log(err));
});

// @route   GET api/users/current
// @desc    Return the current user object
// @access  Private
router.get("/current", passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email,
      phone: req.user.phone,
    });
  }
);

// export the router
module.exports = router;
