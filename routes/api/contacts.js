// imports for the dependencies
const express = require("express");
const passport = require("passport");

// initialize router
const router = express.Router();

// imports for validators
const validateContactInput = require('../../validation/contact');

// imports for database
const Contact = require("../../models/Contact");

// @route   GET api/contacts/all
// @desc    Return all the contacts for the user
// @access  Private
router.get("/all", passport.authenticate("jwt", { session: false }), (req, res) => {
  const errors = {};
  Contact.find({ user: req.user.id })
    .then(contacts => {
      if (!contacts) {
        errors.nocontact = "There are no contacts created";
        return res.status(404).json(errors);
      }
      
      return res.json(contacts);
    })
    .catch(() => {
      errors.nocontact = "There are no contacts created";
      return res.status(404).json(errors);
    });
});

// @route   GET api/contacts/contact/:contact_id
// @desc    Return contact details for one contact
// @access  Private
router.get("/contact/:contact_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  const errors = {};
  Contact.findById({ _id: req.params.contact_id })
    .then(contact => {
      if (!contact) {
        errors.nocontact = "Contact not found";
        return res.status(404).json(errors);
      }

      return res.json(contact);
    })
    .catch(() => {
      errors.nocontact = "Contact not found";
      return res.status(404).json(errors);
    });
});

// @route   POST api/contacts/edit/:contact_id
// @desc    Edit contact
// @access  Private
router.post("/edit/:contact_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  const { errors, isValid } = validateContactInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const contactFields = buildContactFields(req.user, req.body);
  Contact.findByIdAndUpdate(
    { _id: req.params.contact_id },
    { $set: contactFields },
    { new: true }
  )
    .then(contact => res.json(contact))
    .catch(() => {
      errors.nocontact = "Contact not found";
      return res.status(404).json(errors);
    });
});

// @route   POST api/contacts/create
// @desc    Create contact
// @access  Private
router.post("/create", passport.authenticate("jwt", { session: false }), (req, res) => {
  const { errors, isValid } = validateContactInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const contactFields = buildContactFields(req.user, req.body);
  new Contact(contactFields).save().then(contact => res.json(contact));
});

// build contactFields for create and update
const buildContactFields = (user, body) => {
  const contactFields = {};
  // get user details
  contactFields.user = user.id;
  // get name details
  contactFields.name = {};
  contactFields.name.firstName = body.name.firstName;
  if (body.name.middleName) contactFields.name.middleName = body.name.middleName;
  if (body.name.lastName) contactFields.name.lastName = body.name.lastName;
  if (body.name.displayName) contactFields.name.displayName = body.name.displayName;
  // get phone details
  contactFields.primaryNumber = body.primaryNumber;
  if (body.secondaryNumbers) contactFields.secondaryNumbers = body.secondaryNumbers;
  // get address mail and website details
  if (body.address) contactFields.address = body.address;
  if (body.email) contactFields.email = body.email;
  if (body.website) contactFields.website = body.website;
  // get event details
  if (body.events) contactFields.events = body.events;

  return contactFields;
};

// @route   DELETE api/contacts/delete/:contact_id
// @desc    Delete contact
// @access  Private
router.delete("/delete/:contact_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  Contact.findByIdAndDelete({ _id: req.params.contact_id })
    .then(contact => res.json(contact));
});

// @route   GET api/contacts/allEvents
// @desc    Return events registered by the user
// @access  Private
router.get("/allEvents", passport.authenticate("jwt", { session: false }), (req, res) => {
  const errors = {};
  Contact.find({ user: req.user.id }).then(contacts => {
    if (contacts) {
      let eventList = contacts.map((contact) => {
        let events = [];
        const contactName = getContactName(contact.name);
        if (contact.events && contact.events.length > 0) {
          events = contact.events.map(event => {
            return {
              name: contactName,
              eventType: event.eventType,
              eventDate: event.eventDate,
            }
          });
        }
        return events;
      });
      eventList = eventList.flat();
      eventList.sort(eventSorter);
      const today = new Date(Date.now());
      const upcomingEvents = eventList.filter(event => {
        const eventDate = new Date(event.eventDate);
        return eventDate.getMonth() >= today.getMonth() || (eventDate.getMonth() === today.getMonth() && eventDate.getDate >= today.getDate())
      });
      const nextYearEvents = eventList.filter(event => {
        const eventDate = new Date(event.eventDate);
        return eventDate.getMonth() < today.getMonth() || (eventDate.getMonth() === today.getMonth() && eventDate.getDate < today.getDate())
      });
      eventList = upcomingEvents.concat(nextYearEvents);
      eventList = eventList.map(event => {
        const { eventDate, eventType, name } = event;
        return {
          name,
          eventType,
          eventDate: getDisplayDate(eventDate)
        }
      })
      return res.json(eventList);
    }
    errors.noevents = "There are no events"
    return res.status(404).json(errors);
  });
});

const eventSorter = (event1, event2) => {
  const event1Date = new Date(event1.eventDate);
  const event2Date = new Date(event2.eventDate);
  const e1Day = event1Date.getDate();
  const e1Month = event1Date.getMonth();
  const e2Day = event2Date.getDate();
  const e2Month = event2Date.getMonth();

  if (e1Month === e2Month && e1Day === e2Day) return 0;
  if (e1Month === e2Month) {
    return e1Day < e2Day ? -1 : 1;
  }
  return e1Month < e2Month ? -1 : 1;
};

const getContactName = (name => {
  if (name.displayName && name.displayName !== '') return name.displayName;

  let fullName = name.firstName;
  fullName += name.middleName && name.middleName !== '' ? (' ' + name.middleName) : "";
  fullName += name.lastName && name.lastName !== '' ? (' ' + name.lastName) : "";

  return fullName;
});

const getDisplayDate = dateString => {
  let newDateString = "";
  const newDate = new Date(dateString);
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  const month = newDate.getMonth();
  const day = newDate.getDate();
  switch (day) {
    case 1:
    case 21:
    case 31: {
      newDateString = `${day}st `;
      break;
    }
    case 2:
    case 22: {
      newDateString = `${day}nd `;
      break;
    }
    case 3:
    case 23: {
      newDateString = `${day}rd `;
      break;
    }
    default: {
      newDateString = `${day}th `;
      break;
    }
  }
  newDateString += months[month];
  return newDateString;
};

// export the router
module.exports = router;
