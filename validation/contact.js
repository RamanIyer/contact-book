// imports for the dependencies
const Validator = require("validator");
const isEmpty = require("./is-empty");

// export the validator for usage
module.exports = function validateContactInput(data) {
  let errors = {};

  // make sure the fields are present before checking
  data.name.firstName = !isEmpty(data.name.firstName) ? data.name.firstName : '';
  data.primaryNumber.phoneNumber = !isEmpty(data.primaryNumber.phoneNumber.toString()) ? data.primaryNumber.phoneNumber.toString() : '';

  // check for empty values
  if (Validator.isEmpty(data.name.firstName)) {
    errors.firstName = "First Name field is required"
  }

  if (Validator.isEmpty(data.primaryNumber.phoneNumber.toString())) {
    errors.phone = "Primary Number field is required"
  }

  // check for other conditions if the fields are filled
  if (!Validator.isLength(data.name.firstName, { min: 2, max: 30 }) && (!errors.firstName || errors.firstName === "")) {
    errors.firstName = "Name must be between 2 and 30 characters"
  }

  if (!Validator.isLength(data.primaryNumber.phoneNumber.toString(), { min: 10, max: 10 }) &&
    (!errors.phone || errors.phone === "")) {
    errors.phone = "Phone number must be 10 digits"
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}