// imports for the dependencies
const Validator = require("validator");
const isEmpty = require("./is-empty");

// export the validator for usage
module.exports = function validateRegisterInput(data) {
  let errors = {};

  // make sure the fields are present before checking
  data.name = !isEmpty(data.name) ? data.name : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.phone = !isEmpty(data.phone) ? data.phone : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.password2 = !isEmpty(data.password2) ? data.password2 : '';

  // check for empty values
  if (Validator.isEmpty(data.name)) {
    errors.name = "Name field is required"
  }

  if (Validator.isEmpty(data.email)) {
    errors.email = "Email field is required"
  }

  if (Validator.isEmpty(data.phone)) {
    errors.phone = "Phone field is required"
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = "Password field is required"
  }

  if (Validator.isEmpty(data.password2)) {
    errors.password2 = "Confirm Password field is required"
  }

  // check for other conditions if the fields are filled
  if (!Validator.isLength(data.name, { min: 2, max: 30 }) && (!errors.name || errors.name === "")) {
    errors.name = "Name must be between 2 and 30 characters"
  }
  
  if (!Validator.isEmail(data.email) && (!errors.email || errors.email === "")) {
    errors.email = "Email is invalid"
  }
  
  if (!Validator.isLength(data.phone, {min: 10, max: 10}) && (!errors.phone || errors.phone === "")) {
    errors.phone = "Phone number must be 10 digits"
  }
  
  if (!Validator.isLength(data.password, { min: 8, max: 30 }) && (!errors.password || errors.password === "")) {
    errors.password = "Password must be atleast 8 characters"
  }

  if (!Validator.equals(data.password, data.password2) && (!errors.password2|| errors.password2 === "")) {
    errors.password2 = "Passwords must match"
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}