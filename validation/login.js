// imports for the dependencies
const Validator = require("validator");
const isEmpty = require("./is-empty");

// export the validator for usage
module.exports = function validateLoginInput(data) {
  let errors = {};

  // make sure the fields are present before checking
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';

  // check for empty values
  if (Validator.isEmpty(data.email)) {
    errors.email = "Email field is required"
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = "Password field is required"
  }

  // check for other conditions if the fields are filled
  if (!Validator.isEmail(data.email) && (!errors.email || errors.email === "")) {
    errors.email = "Email is invalid"
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}