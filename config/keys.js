// export the keys required for the project
module.exports = {
  // URI of the database
  MONGO_URI:
    "mongodb+srv://admin:admin@contact-book.yo4tj.mongodb.net/contact-book?retryWrites=true&w=majority",
  // secret or key for JWT token
  SECRET_OR_KEY: "secret"
};
